import cv2
import os, sys
import tkinter as tk
from tkinter import filedialog, messagebox
import imutils
import argparse
import multiprocessing as mp
import concurrent.futures
import threading
import time

#constants
THRESHOLD = 100
CPU_CORES = mp.cpu_count()
MODE = sys.argv[1] #sequential or parallel
FRAME_JUMPS = int(sys.argv[2])
IMAGE_RESIZE_W = int(sys.argv[3])

#functions
def raiseWindowMessage(title, message):
    messagebox.showinfo(title, message)

def resizeImage(width, image):

    return imutils.resize(image, width=width)

def getVideoName(pathToVideo):
    whithoutFormatPath = pathToVideo.split('.')[0]

    return whithoutFormatPath.split('/')[-1]

def varianceOfLaplacian(image):
    # compute the Laplacian of the image and then return the focus
    # measure, which is simply the variance of the Laplacian
    return cv2.Laplacian(image, cv2.CV_64F).var()

def detectBlurryness(image, threshold):
    
    #image = cv2.imread(imagePath)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    fm = varianceOfLaplacian(gray)
    isBlurry = False #true|false
    text = "Not Blurry"

    # if the focus measure is less than the supplied threshold,
    # then the image should be considered "blurry"
    if fm < threshold:
        text = "Blurry"
        isBlurry = True

        # show the image
        '''cv2.putText(gray, "{}: {:.2f}".format(text, fm), (10, 80),
        cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 3)
        cv2.imshow("Image", gray)
        key = cv2.waitKey(0)'''

    #print('This image is: {text} | {isb} '.format(text=text, isb=0) )
    return isBlurry

def getThreadLoadWork(totalVideoFrames):
    subFrames = totalVideoFrames//CPU_CORES
    startFrames = []
    limitFrames = []
    initFrames = 1
    limitFrame = subFrames
    for x in range(CPU_CORES):
        startFrames.append(initFrames)
        limitFrames.append(limitFrame)
        initFrames = limitFrame + 1
        limitFrame += subFrames
    return (startFrames, limitFrames)

def parallel(startFrames, limitFrames, file):
    print("CPU cores:{}".format(mp.cpu_count()) )

    threads = []
    for i in range(CPU_CORES):
        threads.append(mp.Process(target=getImagesFromVideo, args=(startFrames[i], limitFrames[i], file), daemon=False) )
    [t.start() for t in threads]
    [t.join() for t in threads]

def getImagesFromVideo(startFrame, limitFrame, file):
    currentFrame = startFrame
    video = cv2.VideoCapture(file)
    videoName = getVideoName(file)
    initFrame, frame = video.read()

    print("*getImagesFromVideo F()*")

    while (initFrame):
        if(currentFrame > limitFrame):
            break
        video.set(1, startFrame) 
        ret, frame = video.read()

        if ret:
            
            if detectBlurryness(frame, THRESHOLD):
                name = './data/{videoName}/frameWithBlurryness{currentFrame}.jpg'.format(videoName=videoName, currentFrame=currentFrame )
            else:
                name = './data/{videoName}/frame{currentFrame}.jpg'.format(videoName=videoName, currentFrame=currentFrame )
            
            cv2.imwrite(name, resizeImage(IMAGE_RESIZE_W, frame))

            currentFrame += FRAME_JUMPS
            startFrame += FRAME_JUMPS
        else:
            break

    video.release()
    cv2.destroyAllWindows()

def convertToImage(file):
    print("ARGS -> mode:{m} jumpFrames:{jump} Resizing Width:{w}".format(m=MODE, jump=FRAME_JUMPS, w=IMAGE_RESIZE_W))
    try:
        videoName = getVideoName(file)
        video = cv2.VideoCapture(file)
        # creating a folder named data
        
        if not os.path.exists('data/{}'.format(videoName) ):
            os.makedirs('data/{}'.format(videoName) )
            print('path created: data/{}'.format(videoName))

    except OSError:
        raiseWindowMessage("Error", "Error: Creating directory of data")

    currentFrame = 0
    initFrame, frame = video.read()
    total = 0
    fps = 0

    if(not initFrame):
        raiseWindowMessage("Error", "Not a video or supported video file.")

        return

    else:
        total = int(video.get(cv2.CAP_PROP_FRAME_COUNT)) #to get frames total of video.
        fps = video.get(cv2.CAP_PROP_FPS)
        print('Total frames of this video: {} '.format(total) )
        print('Video fps: {} '.format(fps) )

    start = time.time()
    if(MODE == "parallel"):
        #startFrames = [1, 1000, 2000]
        #limitFrames = [999, 1999, 2999]

        startFrames, limitFrames = getThreadLoadWork(total)

        parallel(startFrames, limitFrames, file)
    else:
        getImagesFromVideo(1, total, file) #startFrame, limitFrame, file

    end = time.time()
    print("Total time: {sec} seconds in {MODE} mode".format(sec=round( (end - start), 4 ), MODE=MODE ) )

    '''
    limitFrame = 60*fps
    amountJumpsLimitFrames = 60*fps
    print("limit {}".format(limitFrame))
    while (initFrame):
        if(limitFrame > total):
            break
        video.set(1, limitFrame) 
        # reading from frame
        ret, frame = video.read()

        if ret:
            # if video is still left continue creating images
            
            if detectBlurryness(frame, THRESHOLD):
            #name = './data/' + videoName+ '/frame' + str(currentFrame) + '.jpg'
                name = './data/{videoName}/frameWithBlurryness{currentFrame}.jpg'.format(videoName=videoName, currentFrame=currentFrame )
            else:
                name = './data/{videoName}/frame{currentFrame}.jpg'.format(videoName=videoName, currentFrame=currentFrame )
            #print('Creating...' + name)
            
            # writing the extracted images resized
            cv2.imwrite(name, resizeImage(IMAGE_RESIZE_W, frame))
            #cv2.imwrite(name, frame)
            
            # increasing counter so that it will
            # show how many frames are created
            currentFrame += 1
            limitFrame += amountJumpsLimitFrames
            #break
        else:
            break
'''
    # Release all space and windows once done
    video.release()
    cv2.destroyAllWindows()

def main():
    root= tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()

    convertToImage(file_path) if file_path else main()

if __name__ == '__main__':
    main()